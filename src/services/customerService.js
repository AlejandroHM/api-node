import connection from '../database'

export const getCustomers = async() => {
    let query = 'SELECT customer.id_customer,business_name as name,' +
        'latitud as latitude,' +
        'longitud as longitude,' +
        'limite_credito as credit_limit,' +
        'address as fiscal_address,' +
        'nom_comprador as buyer_name,' +
        'email_comprador as buyer_email,' +
        'cel_comprador as buyer_fiscalPhone,' +
        'true as visited,' +
        '1200 as balance,' +
        '0 as procent_method,' +
        'true as scheduled,' +
        'false as aggregate ' +
        'FROM customer ' +
        'INNER JOIN customer_addresses ON customer.direccionprincipal = customer_addresses.id_address ' +
        'WHERE fk_id_salesman=2118';
    return new Promise((resolve, reject) => {
        connection.query(query, (error, results, fields) => {
            if (error) return reject(error); // <- se rechaza la promesa y se pasa el motivo
            console.log('Query correcto.'); // mensaje de control
            return resolve(results); // <- se resuelve la Promesa y se pasa el resultado
        });
    });
}