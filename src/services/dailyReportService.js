import connection from '../database'

export const getCustomers = async() => {
    let query = '';
    return new Promise((resolve, reject) => {
        connection.query(query, (error, results, fields) => {
            if (error) return reject(error); // <- se rechaza la promesa y se pasa el motivo
            console.log('Query correcto.'); // mensaje de control
            return resolve(results); // <- se resuelve la Promesa y se pasa el resultado
        });
    });
}