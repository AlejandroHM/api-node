import * as modelUser from '../models/User'
import jwt from 'jsonwebtoken'
import config from '../config'

export const login = (req, res) => {
    //Se tiene que consultar en base de datos que el usuario exista
    const { id_salesman, password } = req.body
    if (modelUser.User.id_salesman === id_salesman) {
        //Generacion de token
        const token = jwt.sign({ "id_salesman": id_salesman, "password": password }, config.SECRET, {
            expiresIn: 86400
        });
        res.json({ "token": token })
    } else {
        res.json("Authentication Failed")
    }
}