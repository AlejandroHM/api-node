import * as customerService from '../services/customerService'

export const getWallet = async(req, res) => {
    const response = customerService.getCustomers();
    response.then((response) => {
        res.json(response)
    }).catch((err) => {
        res.json({
            "error": err,
            "message": "An error occurred during execution"
        })
    });
}

export const getCustomerInformation = async(req, res) => {
    res.json("Información del cliente")
}

export const getPendingInvoices = async(req, res) => {
    res.json("Traer antiguedad")
}

export const getInvoices = async(req, res) => {
    res.json("Traer consumos")
}

export const savePrepaid = async(req, res) => {
    res.json("Guardar anticipo")
}

export const savePayment = async(req, res) => {
    res.json("guardarPago")
}

export const getPaymentTypes = async(req, res) => {
    res.json("Traer tipos de pago")
}

export const getCustomerAddress = async(req, res) => {
    res.json("Traer dirección")
}

export const getCustomerAddresses = async(req, res) => {
    res.json("Traer direcciónes")
}

export const saveAddress = async(req, res) => {
    res.json("Guardar direcciónes")
}

export const updateAddress = async(req, res) => {
    res.json("Actualizar dirección")
}

export const deleteAddress = async(req, res) => {
    res.json("eliminar dirección")
}

export const getPaymentMethods = async(req, res) => {
    res.json("Traer metodos de pago")
}

export const saveOrder = async(req, res) => {
    res.json("Guardar pedido")
}