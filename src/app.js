import express from 'express'
import morgan from 'morgan'
import orderRoutes from './routes/order.routes'
import authRoutes from './routes/auth.routes'
import customerRoutes from './routes/customer.routes'
import dailyReportRoutes from './routes/dailyReport.routes'
import satRoutes from './routes/sat.routes'
const app = express();

app.use(morgan("dev"));
app.use(express.json());

app.use("/apiwurth/order", orderRoutes)
app.use("/apiwurth/auth", authRoutes)
app.use("/apiwurth/customer", customerRoutes)
app.use("/apiwurth/daily-report", dailyReportRoutes)
app.use("/apiwurth/sat", satRoutes)

export default app;