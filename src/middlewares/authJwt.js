import jwt from 'jsonwebtoken'
import config from '../config'
export const verifyToken = async(req, res, next) => {
    const bearerToken = req.headers["authorization"];
    if (!bearerToken) {
        res.json({ "message": "Access Denied" });
    } else {
        try {
            const userDecoded = jwt.verify(bearerToken.split(" ")[1], config.SECRET);
            next();
        } catch (error) {
            res.json(error);
        }
    }
}