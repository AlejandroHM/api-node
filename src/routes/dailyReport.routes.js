import { Router } from 'express'
const router = Router();
import * as authJwt from '../middlewares/authJwt'
import * as dailyReportController from '../controllers/dailyReport.controller'

router.post("/tracking", /*authJwt.verifyToken,*/ dailyReportController.saveTracking);
router.get("/customers", /*authJwt.verifyToken,*/ dailyReportController.getCustomers);
router.post("/customer", authJwt.verifyToken, dailyReportController.addCustomer);
router.get("/visit/visits", authJwt.verifyToken, dailyReportController.getVisitsCustomer);
router.post("/visit", authJwt.verifyToken, dailyReportController.saveVisit);

export default router;