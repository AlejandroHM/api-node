import { Router } from 'express'
const router = Router();
import * as customerController from '../controllers/customer.controller'
import * as authJwt from '../middlewares/authJwt'

router.get("/customers", /*authJwt.verifyToken,*/ customerController.getWallet);
router.get("/:id_customer", authJwt.verifyToken, customerController.getCustomerInformation);
router.get("/:id_customer/pending-invoices", authJwt.verifyToken, customerController.getPendingInvoices);
router.get("/:id_customer/invoices", authJwt.verifyToken, customerController.getInvoices);
router.post("/:id_customer/invoice/:id_invoice/payment", authJwt.verifyToken, customerController.savePrepaid);
router.post("/:id_customer/payment", authJwt.verifyToken, customerController.savePayment);
router.get("/payment-type", authJwt.verifyToken, customerController.getPaymentTypes);
router.get("/:id_customer/address/:id_address", authJwt.verifyToken, customerController.getCustomerAddress);
router.get("/:id_customer/address/addresses", authJwt.verifyToken, customerController.getCustomerAddresses);
router.post("/:id_customer/address/:id_address", authJwt.verifyToken, customerController.saveAddress);
router.put("/:id_customer/address/:id_address", authJwt.verifyToken, customerController.updateAddress);
router.delete("/:id_customer/address/:id_address", authJwt.verifyToken, customerController.deleteAddress);
router.get("/:id_customer/payment-methods", authJwt.verifyToken, customerController.getPaymentMethods);
router.post("/:id_customer/order", authJwt.verifyToken, customerController.saveOrder);

export default router;