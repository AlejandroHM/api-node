import { Router } from 'express'
const router = Router();
import * as satController from '../controllers/sat.controller'
import * as authJwt from '../middlewares/authJwt'

router.get("/cfdi-use", authJwt.verifyToken, satController.getCfdiUse);
router.get("/payment-method", authJwt.verifyToken, satController.getPaymentMethod);
router.get("/payment-form", authJwt.verifyToken, satController.getPaymentForm);

export default router;