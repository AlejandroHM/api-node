import { Router } from 'express'
const router = Router();
import * as orderController from '../controllers/order.controller'
import * as authJwt from '../middlewares/authJwt'

router.get("/authorization-code", authJwt.verifyToken, orderController.validateAuthorizationCode);
router.get("/authorization-code/customer/:id_customer", authJwt.verifyToken, orderController.getAuthorizationCodes);

export default router;